package com.blt.customerorder.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Validated
@ApiModel(description = "Product reference object to be used in order creation.")
public class ProductRef {

    @NotBlank(message = "Product reference id cannot be empty!")
    @ApiModelProperty(value = "Product id saved in database.", example = "a804c2a2-b923-11ec-8422-0242ac120002", required = true)
    private String id;
}
