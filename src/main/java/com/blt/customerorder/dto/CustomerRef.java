package com.blt.customerorder.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Validated
@ApiModel(description = "Customer reference object to be used in order creation.")
public class CustomerRef {

    @NotBlank(message = "Customer reference id cannot be empty!")
    @ApiModelProperty(value = "Customer id saved in database.", example = "9dffa204-b923-11ec-8422-0242ac120002", required = true)
    private String id;
}
