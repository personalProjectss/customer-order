package com.blt.customerorder.dto;

import com.blt.customerorder.validator.emptyvalidator.NullableButNotEmpty;
import com.blt.customerorder.validator.leastzerovalidator.AtLeastZero;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@Validated
@ApiModel(description = "Product object which will be used in update operation")
public class ProductUpdate {

    @NullableButNotEmpty(message = "Product name field cannot be empty!")
    @ApiModelProperty(value = "Product name. It can be null but cannot be empty string.", example = "Computer")
    private String name;

    @AtLeastZero(message = "Inventory value at least zero!", nullable = true)
    @ApiModelProperty(value = "It's update object of  number of stock . It must be 0 or greater. It can be null", required = true)
    private Integer inventory;

}
