package com.blt.customerorder.dto;

import com.blt.customerorder.validator.leastzerovalidator.AtLeastZero;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Validated
@ApiModel(description = "Product object which will be used in create operation")
public class ProductCreate {

    @NotBlank(message = "Product name field cannot be empty!")
    @ApiModelProperty(value = "Product name", example = "Computer", required = true)
    private String name;

    @AtLeastZero(message = "Inventory value at least zero!")
    @ApiModelProperty(value = "It's number of stock. It must be 0 or greater. It cannot be null", required = true)
    private Integer inventory;

}
