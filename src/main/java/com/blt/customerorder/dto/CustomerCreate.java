package com.blt.customerorder.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@Validated
@ApiModel(description = "Customer object which will be used in create operations")
public class CustomerCreate {

    @NotBlank(message = "Name field cannot be empty!")
    @ApiModelProperty(value = "Customer name", example = "Jhon", required = true)
    private String name;

    @NotBlank(message = "LastName field cannot be empty!")
    @ApiModelProperty(value = "Customer last name", example = "Stone", required = true)
    private String lastName;

    @Email(message = "Invalid email")
    @ApiModelProperty(value = "Customer email", example = "test@email.com", required = true)
    private String email;

    @Pattern(regexp = "(^$|[0-9]{10})", message = "Invalid phone number")
    @ApiModelProperty(value = "Customer email", example = "test@email.com", required = true)
    private String phone;
}
