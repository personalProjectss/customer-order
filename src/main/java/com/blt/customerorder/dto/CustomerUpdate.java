package com.blt.customerorder.dto;

import com.blt.customerorder.validator.emptyvalidator.NullableButNotEmpty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@Validated
@ApiModel(description = "Customer object which will be used in update and create operations")
public class CustomerUpdate {

    @NullableButNotEmpty(message = "Name field cannot be empty!")
    @ApiModelProperty(value = "Customer name", example = "Jhon")
    private String name;

    @NullableButNotEmpty(message = "LastName field cannot be empty!")
    @ApiModelProperty(value = "Customer last name", example = "Stone")
    private String lastName;

    @Email(message = "Invalid email")
    @ApiModelProperty(value = "Customer email", example = "test@email.com")
    private String email;

    @Pattern(regexp = "(^$|[0-9]{10})", message = "Invalid phone number")
    @ApiModelProperty(value = "Customer email", example = "test@email.com")
    private String phone;
}
