package com.blt.customerorder.dto;

import com.blt.customerorder.validator.emptyvalidator.NullableButNotEmpty;
import com.blt.customerorder.validator.leastonevalidator.AtLeastOne;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import java.time.OffsetDateTime;

@Getter
@Setter
@Validated
@ApiModel(description = "Order object which will be used in update operation")
public class OrderUpdate {

    @NullableButNotEmpty(message = "The order name cannot be empty!")
    @ApiModelProperty(value = "Customer name. It can be null but cannot be empty string.", example = "Order-12345")
    private String name;

    @ApiModelProperty(value = "It's order delivery date update object.")
    private OffsetDateTime deliveryDate;

    @AtLeastOne(message = "The number of orders must be at least 1", nullable = true)
    @ApiModelProperty(value = "It's number of order. It must be 1 or greater. It can be null")
    private Integer numberOfOrder;
}
