package com.blt.customerorder.dto;

import com.blt.customerorder.validator.leastonevalidator.AtLeastOne;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Getter
@Setter
@Validated
@ApiModel(description = "Order object which will be used in create operation")
public class OrderCreate {

    @NotBlank(message = "The order name cannot be empty!")
    @ApiModelProperty(value = "Customer name", example = "Order-12345", required = true)
    private String name;

    @NotNull(message = "The order delivery date cannot be empty!")
    @ApiModelProperty(value = "Order delivery date. The delivery date must be after order creation date", required = true)
    private OffsetDateTime deliveryDate;

    @AtLeastOne(message = "The number of orders must be at least 1")
    @ApiModelProperty(value = "It's number of order. It must be 1 or greater. It cannot be null.", required = true)
    private Integer numberOfOrder;

    @Valid
    @NotNull(message = "The order must has a customer")
    @ApiModelProperty(value = "Customer reference data. According to this value, customer validation query will be executed.", required = true)
    private CustomerRef customer;

    @Valid
    @NotNull(message = "The order must associated with a product")
    @ApiModelProperty(value = "Product reference data. According to this value, product validation query will be executed.", required = true)
    private ProductRef product;
}
