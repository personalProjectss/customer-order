package com.blt.customerorder.validator.emptyvalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NullableOrNotEmptyValidator implements ConstraintValidator<NullableButNotEmpty, String> {

    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return value == null || !value.trim().isEmpty();
    }
}
