package com.blt.customerorder.validator.emptyvalidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {NullableOrNotEmptyValidator.class}
)
public @interface NullableButNotEmpty {
    String message() default "can be null but not be empty";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
