package com.blt.customerorder.validator.leastzerovalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AtLeastZeroValidator implements ConstraintValidator<AtLeastZero, Integer> {

    private boolean nullable;

    @Override
    public void initialize(AtLeastZero constraintAnnotation) {
        nullable = constraintAnnotation.nullable();
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null && nullable) {
            return true;
        } else if (value == null) {
            return false;
        }
        return value >= 0;
    }
}
