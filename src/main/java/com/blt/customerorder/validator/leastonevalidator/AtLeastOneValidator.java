package com.blt.customerorder.validator.leastonevalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AtLeastOneValidator implements ConstraintValidator<AtLeastOne, Integer> {

    private boolean nullable;

    @Override
    public void initialize(AtLeastOne constraintAnnotation) {
        nullable = constraintAnnotation.nullable();
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null && nullable) {
            return true;
        } else if (value == null) {
            return false;
        }
        return value >= 1;
    }
}
