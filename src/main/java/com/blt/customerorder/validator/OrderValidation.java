package com.blt.customerorder.validator;

import com.blt.customerorder.entity.Customer;
import com.blt.customerorder.entity.Order;
import com.blt.customerorder.entity.Product;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.repository.CustomerRepository;
import com.blt.customerorder.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;

@Component
@RequiredArgsConstructor
@Transactional
public class OrderValidation {

    private final ProductRepository productRepository;
    private final CustomerRepository customerRepository;

    /**
     * If the delivery date is not after the creation date plus one day, throw an exception.
     *
     * @param deliveryDate The delivery date of the order
     * @param creationDate The date the order was created
     */
    public void orderDeliveryDateValidation(OffsetDateTime deliveryDate, OffsetDateTime creationDate) throws ApiException {
        boolean isAfterCreationDate = deliveryDate.isAfter(creationDate.plusDays(1L));
        if (!isAfterCreationDate) {
            throw new ApiException("Delivery date must be after order creation date", HttpStatus.BAD_REQUEST.value());
        }
    }

    /**
     * Associate an order to a customer.
     *
     * @param order    The order object that you want to associate to the customer.
     * @param customer The customer object that is being associated to the order.
     */
    public void associateWithCustomer(Order order, Customer customer) {
        order.setCustomer(customer);
        customer.getOrder().add(order);
        customerRepository.save(customer);
    }


    /**
     * If the product's inventory is less than the number of orders, throw an exception. Otherwise, subtract the number of
     * orders from the product's inventory, set the product to the order, and save the product
     *
     * @param order   The order object that is being created
     * @param product The product that the order is associated with.
     */
    public void associateWithProduct(Order order, Product product) throws ApiException {
        if ((product.getInventory() - order.getNumberOfOrder()) < 0) {
            throw new ApiException("Not enough items in stock", HttpStatus.BAD_REQUEST.value());
        }
        product.setInventory(product.getInventory() - order.getNumberOfOrder());
        order.setProduct(product);
        product.getOrder().add(order);
        productRepository.save(product);
    }


    /**
     * Check if a customer exists, and if not, throw an exception.
     *
     * @param customerId The id of the customer to be checked.
     * @return A Customer object
     */
    public Customer checkCustomer(String customerId) throws ApiException {
        Customer customer = customerRepository.findById(customerId).orElse(null);
        if (customer == null) {
            throw new ApiException("No customer with id " + customerId, HttpStatus.BAD_REQUEST.value());
        }
        return customer;
    }

    /**
     * If the product is not found, throw an exception.
     *
     * @param productId The id of the product to be checked.
     * @return A product object
     */
    public Product checkProduct(String productId) throws ApiException {
        Product product = productRepository.findById(productId).orElse(null);
        if (product == null) {
            throw new ApiException("No product with id " + productId, HttpStatus.BAD_REQUEST.value());
        }
        return product;
    }
}
