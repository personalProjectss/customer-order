package com.blt.customerorder.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Home redirection to swagger api documentation
 */
@Controller
@ApiIgnore
public class HomeController {
    @GetMapping(value = "/")
    public String redirect() {
        return "redirect:swagger-ui.html";
    }
}
