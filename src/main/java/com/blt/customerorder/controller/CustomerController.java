package com.blt.customerorder.controller;

import com.blt.customerorder.api.CustomerApi;
import com.blt.customerorder.dto.CustomerCreate;
import com.blt.customerorder.dto.CustomerUpdate;
import com.blt.customerorder.entity.Customer;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequiredArgsConstructor
public class CustomerController implements CustomerApi {

    private final CustomerService customerService;

    public ResponseEntity<Customer> createCustomer(CustomerCreate customerCreate) {
        return new ResponseEntity<>(customerService.createCustomer(customerCreate), HttpStatus.CREATED);
    }


    public ResponseEntity<List<Customer>> listCustomer() {
        return new ResponseEntity<>(customerService.listCustomer(), HttpStatus.OK);
    }

    public ResponseEntity<Customer> retrieveCustomer(String id) throws ApiException {
        return new ResponseEntity<>(customerService.retrieveCustomer(id), HttpStatus.OK);
    }

    public ResponseEntity<Void> deleteCustomer(String id) throws ApiException {
        customerService.deleteCustomer(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Customer> updateCustomer(String id, CustomerUpdate customerUpdate) throws ApiException {
        return new ResponseEntity<>(customerService.updateCustomer(id, customerUpdate), HttpStatus.OK);
    }
}
