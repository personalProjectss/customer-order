package com.blt.customerorder.controller;

import com.blt.customerorder.api.OrderApi;
import com.blt.customerorder.dto.OrderCreate;
import com.blt.customerorder.dto.OrderUpdate;
import com.blt.customerorder.entity.Order;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class OrderController implements OrderApi {

    private final OrderService orderService;

    public ResponseEntity<Order> createOrder(OrderCreate orderCreate) throws ApiException {
        return new ResponseEntity<>(orderService.createOrder(orderCreate), HttpStatus.CREATED);
    }

    public ResponseEntity<List<Order>> listOrder() {
        return new ResponseEntity<>(orderService.listOrder(), HttpStatus.OK);
    }

    public ResponseEntity<Order> retrieveOrder(String id) throws ApiException {
        return new ResponseEntity<>(orderService.retrieveOrder(id), HttpStatus.OK);
    }

    public ResponseEntity<Order> retrieveOrderByProduct(String productId) throws ApiException {
        return new ResponseEntity<>(orderService.retrieveOrderByProduct(productId), HttpStatus.OK);
    }

    public ResponseEntity<Order> retrieveOrderByCustomer(String customerId) throws ApiException {
        return new ResponseEntity<>(orderService.retrieveOrderByCustomer(customerId), HttpStatus.OK);
    }

    public ResponseEntity<Void> deleteOrder(String id) throws ApiException {
        orderService.deleteOrder(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Order> updateOrder(String id, OrderUpdate orderUpdate) throws ApiException {
        return new ResponseEntity<>(orderService.updateOrder(id, orderUpdate), HttpStatus.OK);
    }
}
