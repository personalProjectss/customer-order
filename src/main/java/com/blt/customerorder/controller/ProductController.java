package com.blt.customerorder.controller;

import com.blt.customerorder.api.ProductApi;
import com.blt.customerorder.dto.ProductCreate;
import com.blt.customerorder.dto.ProductUpdate;
import com.blt.customerorder.entity.Product;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ProductController implements ProductApi {

    private final ProductService productService;

    public ResponseEntity<Product> createProduct(ProductCreate productCreate) {
        return new ResponseEntity<>(productService.createProduct(productCreate), HttpStatus.CREATED);
    }

    public ResponseEntity<List<Product>> listProduct() {
        return new ResponseEntity<>(productService.listProduct(), HttpStatus.OK);
    }

    public ResponseEntity<Product> retrieveProduct(String id) throws ApiException {
        return new ResponseEntity<>(productService.retrieveProduct(id), HttpStatus.OK);
    }

    public ResponseEntity<Void> deleteProduct(String id) throws ApiException {
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Product> updateProduct(String id, ProductUpdate productUpdate) throws ApiException {
        return new ResponseEntity<>(productService.updateProduct(id, productUpdate), HttpStatus.OK);
    }
}
