package com.blt.customerorder.api;

import com.blt.customerorder.dto.OrderCreate;
import com.blt.customerorder.dto.OrderUpdate;
import com.blt.customerorder.entity.Order;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.util.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "Order Api")
@RequestMapping("/order")
public interface OrderApi {

    @PostMapping(consumes = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Create new order", response = Order.class)
    ResponseEntity<Order> createOrder(@ApiParam(value = "Order object to to used in the create process", required = true)
                                      @RequestBody @Valid OrderCreate orderCreate)
            throws ApiException;

    @GetMapping(produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Retrieve list of created order", response = Order[].class)
    ResponseEntity<List<Order>> listOrder();

    @GetMapping(value = "/{id}", produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Retrieve order", response = Order.class)
    ResponseEntity<Order> retrieveOrder(@ApiParam(value = "Order's id", required = true) @PathVariable String id)
            throws ApiException;

    @GetMapping(value = "product/{productId}", produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Retrieve order data according to product id")
    ResponseEntity<Order> retrieveOrderByProduct(@PathVariable String productId) throws ApiException;

    @GetMapping(value = "customer/{customerId}", produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Retrieve order data according to customer id")
    ResponseEntity<Order> retrieveOrderByCustomer(@PathVariable String customerId) throws ApiException;

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "Delete order")
    ResponseEntity<Void> deleteOrder(@ApiParam(value = "Order's id", required = true) @PathVariable String id)
            throws ApiException;

    @PutMapping(value = "/{id}", consumes = {Constant.APPLICATION_JSON}, produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Update order")
    ResponseEntity<Order> updateOrder(@ApiParam(value = "Order's id", required = true) @PathVariable String id,
                                      @ApiParam(value = "Order object to to used in the update process", required = true)
                                      @RequestBody @Valid OrderUpdate orderUpdate)
            throws ApiException;
}
