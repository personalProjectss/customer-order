package com.blt.customerorder.api;

import com.blt.customerorder.dto.ProductCreate;
import com.blt.customerorder.dto.ProductUpdate;
import com.blt.customerorder.entity.Product;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.util.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "Product Api")
@RequestMapping("/product")
public interface ProductApi {

    @PostMapping(consumes = {Constant.APPLICATION_JSON}, produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Create new product", response = Product.class)
    ResponseEntity<Product> createProduct(@ApiParam(value = "Product object to to used in the create process", required = true)
                                          @RequestBody @Valid ProductCreate productCreate);

    @GetMapping(produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Retrieve list of created product.", response = Product[].class)
    ResponseEntity<List<Product>> listProduct();

    @GetMapping(value = "/{id}", produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Retrieve product", response = Product.class)
    ResponseEntity<Product> retrieveProduct(@ApiParam(value = "Product's id", required = true) @PathVariable String id)
            throws ApiException;

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete product")
    ResponseEntity<Void> deleteProduct(@ApiParam(value = "Product's id", required = true) @PathVariable String id)
            throws ApiException;

    @PutMapping(value = "/{id}", consumes = {Constant.APPLICATION_JSON}, produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Update product", response = Product.class)
    ResponseEntity<Product> updateProduct(@ApiParam(value = "Product's id", required = true) @PathVariable String id,
                                          @ApiParam(value = "Product object to to used in the update process", required = true)
                                          @RequestBody @Valid ProductUpdate productUpdate)
            throws ApiException;
}
