package com.blt.customerorder.api;

import com.blt.customerorder.dto.CustomerCreate;
import com.blt.customerorder.dto.CustomerUpdate;
import com.blt.customerorder.entity.Customer;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.util.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "Customer Api")
@RequestMapping("/customer")
public interface CustomerApi {

    @ApiOperation(value = "Create new customer", response = Customer.class)
    @PostMapping(consumes = {Constant.APPLICATION_JSON}, produces = {Constant.APPLICATION_JSON})
    ResponseEntity<Customer> createCustomer(@ApiParam(value = "Customer object to to used in the create process", required = true)
                                            @RequestBody @Valid CustomerCreate customerCreate);

    @GetMapping(produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Retrieve list of created customer", response = Customer[].class)
    ResponseEntity<List<Customer>> listCustomer();

    @GetMapping(value = "/{id}", produces = {Constant.APPLICATION_JSON})
    @ApiOperation(value = "Retrieve customer", response = Customer.class)
    ResponseEntity<Customer> retrieveCustomer(@ApiParam(value = "Customer's id", required = true) @PathVariable String id)
            throws ApiException;

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete customer")
    ResponseEntity<Void> deleteCustomer(@ApiParam(value = "Customer's id", required = true) @PathVariable String id)
            throws ApiException;

    @ApiOperation(value = "Update customer", response = Customer.class)
    @PutMapping(value = "/{id}", consumes = {Constant.APPLICATION_JSON}, produces = {Constant.APPLICATION_JSON})
    ResponseEntity<Customer> updateCustomer(@ApiParam(value = "Customer's id", required = true) @PathVariable String id,
                                            @ApiParam(value = "Customer object to to used in the update process", required = true)
                                            @RequestBody @Valid CustomerUpdate customerUpdate)
            throws ApiException;
}
