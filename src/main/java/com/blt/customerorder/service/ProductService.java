package com.blt.customerorder.service;

import com.blt.customerorder.dto.ProductCreate;
import com.blt.customerorder.dto.ProductUpdate;
import com.blt.customerorder.entity.Product;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.helper.JsonMergeOperation;
import com.blt.customerorder.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductService {

    private static final String NO_PRODUCT_WITH_ID = "No product with id ";

    private final ProductRepository productRepository;
    private final ObjectMapper objectMapper;
    private final JsonMergeOperation mergeOperation;

    @Transactional
    public Product createProduct(ProductCreate productDto) {
        Product product = objectMapper.convertValue(productDto, Product.class);
        product.setId(UUID.randomUUID().toString());
        return productRepository.save(product);
    }

    public List<Product> listProduct() {
        return productRepository.findAll();
    }

    public Product retrieveProduct(String id) throws ApiException {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) {
            throw new ApiException(NO_PRODUCT_WITH_ID + id, HttpStatus.NOT_FOUND.value());
        }
        return product;
    }

    public void deleteProduct(String id) throws ApiException {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) {
            throw new ApiException(NO_PRODUCT_WITH_ID + id, HttpStatus.NOT_FOUND.value());
        }
        productRepository.deleteById(id);
    }

    public Product updateProduct(String id, ProductUpdate productUpdate) throws ApiException {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) {
            throw new ApiException(NO_PRODUCT_WITH_ID + id, HttpStatus.NOT_FOUND.value());
        }
        Product mergedProduct = mergeOperation.mergePatch(productUpdate, product);
        return productRepository.save(mergedProduct);
    }
}
