package com.blt.customerorder.service;

import com.blt.customerorder.dto.CustomerCreate;
import com.blt.customerorder.dto.CustomerUpdate;
import com.blt.customerorder.entity.Customer;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.helper.JsonMergeOperation;
import com.blt.customerorder.repository.CustomerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class CustomerService {

    private static final String NO_CUSTOMER_WITH_ID = "No customer with id ";

    private final CustomerRepository customerRepository;
    private final ObjectMapper objectMapper;
    private final JsonMergeOperation mergeOperation;

    public Customer createCustomer(CustomerCreate customerCreate) {
        Customer customer = objectMapper.convertValue(customerCreate, Customer.class);
        customer.setId(UUID.randomUUID().toString());
        customer.setCreateDate(OffsetDateTime.now());
        customer.setUpdateDate(OffsetDateTime.now());
        return customerRepository.save(customer);
    }

    public List<Customer> listCustomer() {
        return customerRepository.findAll();
    }

    public Customer retrieveCustomer(String id) throws ApiException {
        Customer customer = customerRepository.findById(id).orElse(null);
        if (customer == null) {
            throw new ApiException(NO_CUSTOMER_WITH_ID + id, HttpStatus.NOT_FOUND.value());
        }
        return customer;
    }

    public void deleteCustomer(String id) throws ApiException {
        Customer customer = customerRepository.findById(id).orElse(null);
        if (customer == null) {
            throw new ApiException(NO_CUSTOMER_WITH_ID + id, HttpStatus.NOT_FOUND.value());
        }
        customerRepository.deleteById(id);
    }

    public Customer updateCustomer(String id, CustomerUpdate customerUpdate) throws ApiException {
        Customer customer = customerRepository.findById(id).orElse(null);
        if (customer == null) {
            throw new ApiException(NO_CUSTOMER_WITH_ID + id, HttpStatus.NOT_FOUND.value());
        }
        Customer mergedCustomer = mergeOperation.mergePatch(customerUpdate, customer);
        mergedCustomer.setUpdateDate(OffsetDateTime.now());
        return mergedCustomer;
    }
}
