package com.blt.customerorder.service;

import com.blt.customerorder.dto.OrderCreate;
import com.blt.customerorder.dto.OrderUpdate;
import com.blt.customerorder.entity.Customer;
import com.blt.customerorder.entity.Order;
import com.blt.customerorder.entity.Product;
import com.blt.customerorder.exception.ApiException;
import com.blt.customerorder.helper.JsonMergeOperation;
import com.blt.customerorder.repository.OrderRepository;
import com.blt.customerorder.validator.OrderValidation;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderService {

    private static final String NO_ORDER_WITH_ID = "No order with id ";

    private final OrderRepository orderRepository;
    private final ObjectMapper objectMapper;
    private final JsonMergeOperation mergeOperation;
    private final OrderValidation orderValidation;

    public Order createOrder(OrderCreate orderCreate) throws ApiException {
        Order order = objectMapper.convertValue(orderCreate, Order.class);
        order.setId(UUID.randomUUID().toString());
        order.setCreateDate(OffsetDateTime.now());

        orderValidation.orderDeliveryDateValidation(order.getDeliveryDate(), order.getCreateDate());

        Customer customer = orderValidation.checkCustomer(order.getCustomer().getId());
        Product product = orderValidation.checkProduct(order.getProduct().getId());

        orderValidation.associateWithProduct(order, product);
        orderValidation.associateWithCustomer(order, customer);
        return orderRepository.save(order);
    }

    public Order retrieveOrder(String id) throws ApiException {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            throw new ApiException(NO_ORDER_WITH_ID + id, HttpStatus.NOT_FOUND.value());
        }
        return order;
    }

    public Order retrieveOrderByCustomer(String customerId) throws ApiException {
        Order order = orderRepository.findByCustomerId(customerId).orElse(null);
        if (order == null) {
            throw new ApiException("No order with customer id " + customerId, HttpStatus.NOT_FOUND.value());
        }
        return order;
    }

    public Order retrieveOrderByProduct(String productId) throws ApiException {
        Order order = orderRepository.findByProductId(productId).orElse(null);
        if (order == null) {
            throw new ApiException("No order with product id " + productId, HttpStatus.NOT_FOUND.value());
        }
        return order;
    }

    public List<Order> listOrder() {
        return orderRepository.findAll();
    }

    public void deleteOrder(String id) throws ApiException {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            throw new ApiException(NO_ORDER_WITH_ID + id, HttpStatus.NOT_FOUND.value());
        }
        orderRepository.deleteById(id);
    }


    public Order updateOrder(String id, OrderUpdate orderUpdate) throws ApiException {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            throw new ApiException(NO_ORDER_WITH_ID + id, HttpStatus.NOT_FOUND.value());
        }
        Order mergedOrder = mergeOperation.mergePatch(orderUpdate, order);
        orderValidation.orderDeliveryDateValidation(mergedOrder.getDeliveryDate(), mergedOrder.getCreateDate());
        Product product = orderValidation.checkProduct(mergedOrder.getProduct().getId());
        orderValidation.associateWithProduct(mergedOrder, product);
        return orderRepository.save(mergedOrder);
    }
}
