package com.blt.customerorder.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "APP_PRODUCT")
@Getter
@Setter
@ApiModel(description = "Product object to save in database")
public class Product extends Auditable {

    @Id
    @ApiModelProperty(value = "Product object id. This field will be set by service during saving.",
            example = "b0475c94-b924-11ec-8422-0242ac120002", required = true)
    private String id;

    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "Product name", example = "Computer", required = true)
    private String name;

    @Column(name = "inventory", nullable = false)
    @ApiModelProperty(value = "It's number of stock. It must be 0 or greater. It cannot be null", required = true)
    private Integer inventory;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnore
    @ApiModelProperty(value = "Associated order list.")
    private List<Order> order = new ArrayList<>();

}
