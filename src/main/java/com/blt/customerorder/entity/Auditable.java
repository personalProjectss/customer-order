package com.blt.customerorder.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.OffsetDateTime;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "Base audit model to track date and owner of the update and create.")
public class Auditable {

    @CreatedDate
    @ApiModelProperty(value = "Model create date. This field is set automatically during saving.", required = true)
    private OffsetDateTime createdDate;

    @LastModifiedDate
    @ApiModelProperty(value = "Model update date. This field is set automatically during saving.", required = true)
    private OffsetDateTime updatedDate;

    @CreatedBy
    @ApiModelProperty(value = "Identity of the person who created the model. This field is set automatically during saving.", required = true)
    private String createdBy;

    @LastModifiedBy
    @ApiModelProperty(value = "Identity of the person who updated the model. This field is set automatically during saving.", required = true)
    private String updatedBy;
}
