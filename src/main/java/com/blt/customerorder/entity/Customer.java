package com.blt.customerorder.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "APP_CUSTOMER")
@Getter
@Setter
@ApiModel(description = "Customer object to save in database.")
public class Customer {

    @Id
    @ApiModelProperty(value = "Customer object id. This field will be set by service during saving.",
            example = "b07d8096-b922-11ec-8422-0242ac120002", required = true)
    private String id;

    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "Customer name", example = "Jhon", required = true)
    private String name;

    @Column(name = "lastName", nullable = false)
    @ApiModelProperty(value = "Customer last name", example = "Stone", required = true)
    private String lastName;

    @Column(name = "email", nullable = false)
    @ApiModelProperty(value = "Customer email", example = "test@email.com", required = true)
    private String email;

    @Column(name = "phone", nullable = false)
    @ApiModelProperty(value = "Customer email", example = "test@email.com", required = true)
    private String phone;

    @Column(name = "createDate", nullable = false)
    @ApiModelProperty(value = "Customer create date. This field is set automatically during saving.", required = true)
    private OffsetDateTime createDate;

    @Column(name = "updateDate", nullable = false)
    @ApiModelProperty(value = "Customer update date. This field is set automatically during saving.", required = true)
    private OffsetDateTime updateDate;

    @Valid
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnore
    @ApiModelProperty(value = "Associated order list.")
    private List<Order> order = new ArrayList<>();
}
