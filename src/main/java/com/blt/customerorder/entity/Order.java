package com.blt.customerorder.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "APP_ORDER")
@Getter
@Setter
@ApiModel(description = "Order object to save in database")
public class Order {

    @Id
    @ApiModelProperty(value = "Order object id. This field will be set by service during saving.",
            example = "d7284cde-b923-11ec-8422-0242ac120002", required = true)
    private String id;

    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "Customer name", example = "Order-12345", required = true)
    private String name;

    @Column(name = "createDate", nullable = false)
    @ApiModelProperty(value = "Order creation date. This field is set automatically during saving.", required = true)
    private OffsetDateTime createDate;

    @Column(name = "deliveryDate", nullable = false)
    @ApiModelProperty(value = "Order delivery date. The delivery date must be after order creation date", required = true)
    private OffsetDateTime deliveryDate;

    @Column(name = "numberOfOrder", nullable = false)
    @ApiModelProperty(value = "It's number of order. It must be 1 or greater. It cannot be null.", required = true)
    private Integer numberOfOrder;

    @ManyToOne
    @JoinColumn(name = "customer_id", foreignKey = @ForeignKey(name = "fk_order_ref_customer"))
    @ApiModelProperty(value = "Customer object to create foreign key in database", required = true)
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "product_id", foreignKey = @ForeignKey(name = "fk_order_ref_product"))
    @ApiModelProperty(value = "Product object to create foreign key in database", required = true)
    private Product product;
}
