package com.blt.customerorder.config;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AuditorAwareImpl implements AuditorAware<String> {

    private static final String DEFAULT_USER = "Product Owner";

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(DEFAULT_USER);
    }
}
