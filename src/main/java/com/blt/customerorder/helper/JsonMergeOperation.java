package com.blt.customerorder.helper;

import com.blt.customerorder.exception.ApiException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class JsonMergeOperation {

    private final ObjectMapper objectMapper;

    @SuppressWarnings({"unchecked"})
    public <T> T mergePatch(Object source, T target) throws ApiException {
        JsonNode mergedNode;
        try {
            JsonMergePatch jsonMergePatch = JsonMergePatch.fromJson(objectMapper.convertValue(source, JsonNode.class));
            mergedNode = jsonMergePatch.apply(objectMapper.convertValue(target, JsonNode.class));
        } catch (JsonPatchException e) {
            throw new ApiException(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY.value());
        }
        return (T) objectMapper.convertValue(mergedNode, target.getClass());
    }
}
