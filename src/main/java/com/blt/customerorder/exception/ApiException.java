package com.blt.customerorder.exception;

import lombok.Getter;

@Getter
public class ApiException extends Exception {

    private final String message;
    private final Integer status;

    public ApiException(String message, Integer status) {
        super(message);
        this.message = message;
        this.status = status;
    }

}
