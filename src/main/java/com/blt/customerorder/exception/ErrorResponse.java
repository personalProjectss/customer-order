package com.blt.customerorder.exception;

import lombok.Getter;

import java.time.OffsetDateTime;

@Getter
public class ErrorResponse {

    private final Long timestamp;

    private final Integer status;

    private final String message;

    public ErrorResponse(Integer status, String message) {
        this.timestamp = OffsetDateTime.now().toInstant().toEpochMilli();
        this.status = status;
        this.message = message;
    }
}
