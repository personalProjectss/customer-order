package com.blt.customerorder.repository;

import com.blt.customerorder.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, String> {

    Optional<Order> findByCustomerId(String customerId);
    Optional<Order> findByProductId(String productId);
}
