package com.blt.customerorder.controller;

import com.blt.customerorder.TestBase;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.OffsetDateTime;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("junit")
class OrderControllerTest extends TestBase {

    private static final String DATA_CREATE = "data/orderData.json";
    private static final String DATA_UPDATE = "data/orderUpdateData.json";
    private static final String PRODUCT_DATA_CREATE = "data/productData.json";
    private static final String CUSTOMER_DATA_CREATE = "data/customerData.json";

    private static final String ORDER_PRODUCT_JSON_PATH = "$.product.id";
    private static final String ORDER_CUSTOMER_JSON_PATH = "$.customer.id";
    private static final String ORDER_DELIVERY_DATE = "$.deliveryDate";
    private static final String ORDER_NUMBER = "$.numberOfOrder";

    @Test
    void givenCorrectOrderData_whenPostOperation_thenReturnSuccess() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));

        // Expected order name
        String expectedName = JsonPath.read(orderData, JSON_PATH_NAME);

        // Start create operation
        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(HttpStatus.CREATED.value()),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_PATH_ID).isString(),
                        MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(expectedName)
                ));
    }

    @Test
    void givenEmptyNameInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .set(JSON_PATH_NAME, EMPTY_STRING)
                .jsonString();

        // Expected error response
        String expectedMessage = "The order name cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNullDeliveryDateInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .delete(ORDER_DELIVERY_DATE)
                .jsonString();

        // Expected error response
        String expectedMessage = "The order delivery date cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenWrongDeliveryDateInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .set(ORDER_DELIVERY_DATE, OffsetDateTime.now().minusYears(1).toString())
                .jsonString();

        // Expected error response
        String expectedMessage = "Delivery date must be after order creation date";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenWrongOrderNumberInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .set(ORDER_NUMBER, 0)
                .jsonString();

        // Expected error response
        String expectedMessage = "The number of orders must be at least 1";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNullOrderNumberInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .delete(ORDER_NUMBER)
                .jsonString();

        // Expected error response
        String expectedMessage = "The number of orders must be at least 1";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenEmptyProductIdInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .set(ORDER_PRODUCT_JSON_PATH, EMPTY_STRING)
                .jsonString();

        // Expected error response
        String expectedMessage = "Product reference id cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNotProductRefInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .set(ORDER_PRODUCT_JSON_PATH, UNDEFINED_ID)
                .jsonString();

        // Expected error response
        String expectedMessage = "No product with id";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNullProductRefInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .delete("$.product")
                .jsonString();

        // Expected error response
        String expectedMessage = "The order must associated with a product";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenEmptyCustomerIdInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .set(ORDER_CUSTOMER_JSON_PATH, EMPTY_STRING)
                .jsonString();

        // Expected error response
        String expectedMessage = "Customer reference id cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNotCustomerRefInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .set(ORDER_CUSTOMER_JSON_PATH, UNDEFINED_ID)
                .jsonString();

        // Expected error response
        String expectedMessage = "No customer with id";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNullCustomerRefInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .delete("$.customer")
                .jsonString();

        // Expected error response
        String expectedMessage = "The order must has a customer";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNullNameInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .delete(JSON_PATH_NAME)
                .jsonString();

        // Expected error response
        String expectedMessage = "The order name cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenUndefinedFieldInOrderData_whenPostOperation_thenReturnError() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));
        orderData = JsonPath
                .parse(orderData)
                .put("$", "dummyField", "dummyValue")
                .jsonString();

        // Expected error response
        String expectedMessage = "JSON parse error: Unrecognized field";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenStockLimitExceeded_whenPostOperation_thenReturnSuccess() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));

        // Expected error response
        String expectedMessage = "Not enough items in stock";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        // Create first order
        createPOSTRequest(ORDER_URL, orderData)
                .andDo(mvcResult -> {

                    // Create second order
                    createPOSTRequest(ORDER_URL, orderData)
                            .andDo(res ->

                                    // Create third order and it will be exceeded stock limit
                                    mvc.perform(MockMvcRequestBuilders.post(ORDER_URL)
                                            .header(ACCEPT, APPLICATION_JSON)
                                            .header(CONTENT_TYPE, APPLICATION_JSON)
                                            .content(orderData))
                                            .andExpect(ResultMatcher.matchAll(
                                                    result -> System.out.println(result.getResponse().getContentAsString()),
                                                    status().is(expectedStatus),
                                                    MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                                                    MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                                                    MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                                            )));
                });
    }

    @Test
    void givenCreatedOrder_whenGetOperation_thenReturnSuccess() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));

        createPOSTRequest(ORDER_URL, orderData)
                .andDo(mvcResult -> {
                    String orderId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    String orderName = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_NAME);
                    mvc.perform(MockMvcRequestBuilders.get(ORDER_URL + SLASH + orderId)
                            .header(ACCEPT, APPLICATION_JSON)
                            .header(CONTENT_TYPE, APPLICATION_JSON))
                            .andExpect(ResultMatcher.matchAll(
                                    status().is(HttpStatus.OK.value()),
                                    status().is2xxSuccessful(),
                                    header().exists(HttpHeaders.CONTENT_TYPE),
                                    jsonPath(JSON_PATH, notNullValue()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_ID).value(orderId),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(orderName)
                            ));
                });
    }

    @Test
    void givenCreatedOrder_whenGetOperationWithProductId_thenReturnSuccess() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));

        createPOSTRequest(ORDER_URL, orderData)
                .andDo(mvcResult -> {
                    String orderId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    String orderName = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_NAME);

                    String productId = JsonPath.read(mvcResult.getResponse().getContentAsString(), ORDER_PRODUCT_JSON_PATH);

                    mvc.perform(MockMvcRequestBuilders.get(ORDER_URL + PRODUCT_URL + SLASH + productId)
                            .header(ACCEPT, APPLICATION_JSON)
                            .header(CONTENT_TYPE, APPLICATION_JSON))
                            .andExpect(ResultMatcher.matchAll(
                                    status().is(HttpStatus.OK.value()),
                                    status().is2xxSuccessful(),
                                    header().exists(HttpHeaders.CONTENT_TYPE),
                                    jsonPath(JSON_PATH, notNullValue()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_ID).value(orderId),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(orderName)
                            ));
                });
    }

    @Test
    void givenCreatedOrder_whenGetOperationWithNoCreatedProductId_thenReturnError() throws Exception {
        String expectedMessage = "No order with product id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.get(ORDER_URL + PRODUCT_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedOrder_whenGetOperationWithCustomerId_thenReturnSuccess() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));

        createPOSTRequest(ORDER_URL, orderData)
                .andDo(mvcResult -> {
                    String orderId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    String orderName = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_NAME);

                    String customerId = JsonPath.read(mvcResult.getResponse().getContentAsString(), ORDER_CUSTOMER_JSON_PATH);

                    mvc.perform(MockMvcRequestBuilders.get(ORDER_URL + CUSTOMER_URL + SLASH + customerId)
                            .header(ACCEPT, APPLICATION_JSON)
                            .header(CONTENT_TYPE, APPLICATION_JSON))
                            .andExpect(ResultMatcher.matchAll(
                                    status().is(HttpStatus.OK.value()),
                                    status().is2xxSuccessful(),
                                    header().exists(HttpHeaders.CONTENT_TYPE),
                                    jsonPath(JSON_PATH, notNullValue()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_ID).value(orderId),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(orderName)
                            ));
                });
    }

    @Test
    void givenCreatedOrder_whenGetOperationWithNoCreatedCustomerId_thenReturnError() throws Exception {
        String expectedMessage = "No order with customer id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.get(ORDER_URL + CUSTOMER_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNotCreatedOrder_whenGetOperation_thenReturnError() throws Exception {
        String expectedMessage = "No order with id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.get(ORDER_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedOrder_whenDeleteOperation_thenReturnSuccess() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));

        createPOSTRequest(ORDER_URL, orderData)
                .andDo(mvcResult -> {
                    String orderId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    mvc.perform(MockMvcRequestBuilders.delete(ORDER_URL + SLASH + orderId))
                            .andExpect(ResultMatcher.matchAll(
                                    status().is(HttpStatus.NO_CONTENT.value()),
                                    status().is2xxSuccessful()
                            ));
                });
    }

    @Test
    void givenNotCreatedOrder_whenDeleteOperation_thenReturnError() throws Exception {
        String expectedMessage = "No order with id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.delete(ORDER_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedOrder_whenPutOperation_thenReturnSuccess() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));

        createPOSTRequest(ORDER_URL, orderData)
                .andDo(mvcResult -> {
                    String orderId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    String orderUpdateData = loadJsonData(DATA_UPDATE);
                    String expectedName = "name" + UUID.randomUUID().toString();
                    orderUpdateData = JsonPath.parse(orderUpdateData)
                            .set(ORDER_DELIVERY_DATE, OffsetDateTime.now().plusYears(1L).toString())
                            .set(JSON_PATH_NAME, expectedName)
                            .delete(ORDER_NUMBER)
                            .jsonString();

                    mvc.perform(MockMvcRequestBuilders.put(ORDER_URL + SLASH + orderId)
                            .header(ACCEPT, APPLICATION_JSON)
                            .header(CONTENT_TYPE, APPLICATION_JSON)
                            .content(orderUpdateData))
                            .andExpect(ResultMatcher.matchAll(
                                    result -> System.out.println(result.getResponse().getContentAsString()),
                                    status().is(HttpStatus.OK.value()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_ID).value(orderId),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(expectedName)
                            ));
                });
    }

    @Test
    void givenNotCreatedOrder_whenPutOperation_thenReturnError() throws Exception {
        String orderUpdateData = loadJsonData(DATA_UPDATE);
        orderUpdateData = JsonPath.parse(orderUpdateData)
                .set(ORDER_DELIVERY_DATE, OffsetDateTime.now().plusYears(1L).toString())
                .jsonString();


        String expectedMessage = "No order with id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.put(ORDER_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(orderUpdateData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedOrderList_whenGetListOperation_thenReturnSuccess() throws Exception {
        String orderData = prepareOrderData(loadJsonData(DATA_CREATE));

        // Create first order
        createPOSTRequest(ORDER_URL, orderData)
                .andDo(mvcResult -> {

                    // Create second order
                    createPOSTRequest(ORDER_URL, orderData)
                            .andDo(mvcRes -> mvc.perform(MockMvcRequestBuilders.get(ORDER_URL)
                                    .header(ACCEPT, APPLICATION_JSON)
                                    .header(CONTENT_TYPE, APPLICATION_JSON))
                                    .andExpect(ResultMatcher.matchAll(
                                            status().is(HttpStatus.OK.value()),
                                            status().is2xxSuccessful(),
                                            header().exists(HttpHeaders.CONTENT_TYPE),
                                            jsonPath(JSON_PATH, notNullValue()),
                                            jsonPath(JSON_PATH_ARRAY, hasSize(2))
                                    )));
                });
    }

    private String prepareOrderData(String orderData) throws Exception {
        String productId = createProduct();
        String customerId = createCustomer();

        return JsonPath.parse(orderData)
                .set(ORDER_DELIVERY_DATE, OffsetDateTime.now().plusYears(1L).toString())
                .set(ORDER_NUMBER, 1)
                .set(ORDER_PRODUCT_JSON_PATH, productId)
                .set(ORDER_CUSTOMER_JSON_PATH, customerId)
                .jsonString();
    }

    private String createProduct() throws Exception {
        String productData = loadJsonData(PRODUCT_DATA_CREATE);
        productData = JsonPath.parse(productData).set("$.inventory", 2).jsonString();
        MvcResult mvcResult = createPOSTRequest(PRODUCT_URL, productData).andReturn();
        return JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
    }

    private String createCustomer() throws Exception {
        String productData = loadJsonData(CUSTOMER_DATA_CREATE);
        MvcResult mvcResult = createPOSTRequest(CUSTOMER_URL, productData).andReturn();
        return JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
    }

}
