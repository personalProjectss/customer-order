package com.blt.customerorder.controller;

import com.blt.customerorder.TestBase;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("junit")
class CustomerControllerTest extends TestBase {

    private static final String DATA_CREATE = "data/customerData.json";

    @Test
    void givenCorrectCustomerData_whenPostOperation_thenReturnSuccess() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);

        // Expected customer name
        String expectedName = JsonPath.read(customerData, JSON_PATH_NAME);

        // Start create operation
        mvc.perform(MockMvcRequestBuilders.post(CUSTOMER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(customerData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(HttpStatus.CREATED.value()),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_PATH_ID).isString(),
                        MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(expectedName)
                ));
    }

    @Test
    void givenEmptyNameInCustomerData_whenPostOperation_thenReturnError() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);
        customerData = JsonPath
                .parse(customerData)
                .set(JSON_PATH_NAME, EMPTY_STRING)
                .jsonString();

        // Expected error response
        String expectedMessage = "Name field cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(CUSTOMER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(customerData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenEmptyLastNameInCustomerData_whenPostOperation_thenReturnError() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);
        customerData = JsonPath
                .parse(customerData)
                .set(JSON_PATH_lAST_NAME, EMPTY_STRING)
                .jsonString();

        // Expected error response
        String expectedMessage = "LastName field cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(CUSTOMER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(customerData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenEmptyEmailInCustomerData_whenPostOperation_thenReturnError() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);
        customerData = JsonPath
                .parse(customerData)
                .set(JSON_PATH_EMAIL, EMPTY_STRING)
                .jsonString();

        // Expected error response
        String expectedMessage = "Invalid email";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(CUSTOMER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(customerData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenEmptyPhoneInCustomerData_whenPostOperation_thenReturnError() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);
        customerData = JsonPath
                .parse(customerData)
                .set(JSON_PATH_PHONE, EMPTY_STRING)
                .jsonString();

        // Expected error response
        String expectedMessage = "Invalid phone number";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(CUSTOMER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(customerData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNullNameInCustomerData_whenPostOperation_thenReturnError() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);
        customerData = JsonPath
                .parse(customerData)
                .delete(JSON_PATH_NAME)
                .jsonString();

        // Expected error response
        String expectedMessage = "Name field cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(CUSTOMER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(customerData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenUndefinedFieldInCustomerData_whenPostOperation_thenReturnError() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);
        customerData = JsonPath
                .parse(customerData)
                .put("$", "dummyField", "dummyValue")
                .jsonString();

        // Expected error response
        String expectedMessage = "JSON parse error: Unrecognized field";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(CUSTOMER_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(customerData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedCustomer_whenGetOperation_thenReturnSuccess() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);

        createPOSTRequest(CUSTOMER_URL, customerData)
                .andDo(mvcResult -> {
                    String customerId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    String customerName = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_NAME);
                    mvc.perform(MockMvcRequestBuilders.get(CUSTOMER_URL + SLASH + customerId)
                            .header(ACCEPT, APPLICATION_JSON)
                            .header(CONTENT_TYPE, APPLICATION_JSON))
                            .andExpect(ResultMatcher.matchAll(
                                    status().is(HttpStatus.OK.value()),
                                    status().is2xxSuccessful(),
                                    header().exists(HttpHeaders.CONTENT_TYPE),
                                    jsonPath(JSON_PATH, notNullValue()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_ID).value(customerId),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(customerName)
                            ));
                });
    }

    @Test
    void givenNotCreatedCustomer_whenGetOperation_thenReturnError() throws Exception {
        String expectedMessage = "No customer with id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.get(CUSTOMER_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedCustomer_whenDeleteOperation_thenReturnSuccess() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);

        createPOSTRequest(CUSTOMER_URL, customerData)
                .andDo(mvcResult -> {
                    String customerId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    mvc.perform(MockMvcRequestBuilders.delete(CUSTOMER_URL + SLASH + customerId))
                            .andExpect(ResultMatcher.matchAll(
                                    status().is(HttpStatus.NO_CONTENT.value()),
                                    status().is2xxSuccessful()
                            ));
                });
    }

    @Test
    void givenNotCreatedCustomer_whenDeleteOperation_thenReturnError() throws Exception {
        String expectedMessage = "No customer with id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.delete(CUSTOMER_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedCustomer_whenPutOperation_thenReturnSuccess() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);

        createPOSTRequest(CUSTOMER_URL, customerData)
                .andDo(mvcResult -> {
                    String customerId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    String customerUpdateData = loadJsonData(DATA_CREATE);
                    String expectedName = "name" + UUID.randomUUID().toString();
                    customerUpdateData = JsonPath.parse(customerUpdateData).set(JSON_PATH_NAME, expectedName).jsonString();

                    mvc.perform(MockMvcRequestBuilders.put(CUSTOMER_URL + SLASH + customerId)
                            .header(ACCEPT, APPLICATION_JSON)
                            .header(CONTENT_TYPE, APPLICATION_JSON)
                            .content(customerUpdateData))
                            .andExpect(ResultMatcher.matchAll(
                                    result -> System.out.println(result.getResponse().getContentAsString()),
                                    status().is(HttpStatus.OK.value()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_ID).value(customerId),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(expectedName)
                            ));
                });
    }

    @Test
    void givenNotCreatedCustomer_whenPutOperation_thenReturnError() throws Exception {
        String customerUpdateData = loadJsonData(DATA_CREATE);

        String expectedMessage = "No customer with id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.put(CUSTOMER_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(customerUpdateData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedCustomerList_whenGetListOperation_thenReturnSuccess() throws Exception {
        String customerData = loadJsonData(DATA_CREATE);

        // Create first customer
        createPOSTRequest(CUSTOMER_URL, customerData)
                .andDo(mvcResult -> {

                    // Create second customer
                    createPOSTRequest(CUSTOMER_URL, customerData)
                            .andDo(mvcRes -> mvc.perform(MockMvcRequestBuilders.get(CUSTOMER_URL)
                                    .header(ACCEPT, APPLICATION_JSON)
                                    .header(CONTENT_TYPE, APPLICATION_JSON))
                                    .andExpect(ResultMatcher.matchAll(
                                            status().is(HttpStatus.OK.value()),
                                            status().is2xxSuccessful(),
                                            header().exists(HttpHeaders.CONTENT_TYPE),
                                            jsonPath(JSON_PATH, notNullValue()),
                                            jsonPath(JSON_PATH_ARRAY, hasSize(2))
                                    )));
                });
    }

}
