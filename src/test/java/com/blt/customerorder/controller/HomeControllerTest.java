package com.blt.customerorder.controller;

import com.blt.customerorder.TestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("junit")
class HomeControllerTest extends TestBase {

    @Test
    void whenGetOperation_returnSuccess() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(SLASH))
                .andExpect(ResultMatcher.matchAll(
                        status().is(HttpStatus.FOUND.value())
                ));
    }

}
