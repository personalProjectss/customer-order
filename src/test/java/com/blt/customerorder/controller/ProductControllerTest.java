package com.blt.customerorder.controller;

import com.blt.customerorder.TestBase;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("junit")
class ProductControllerTest extends TestBase {

    private static final String DATA_CREATE = "data/productData.json";
    private static final String JSON_PATH_INVENTORY = "$.inventory";

    @Test
    void givenCorrectProductData_whenPostOperation_thenReturnSuccess() throws Exception {
        String productData = loadJsonData(DATA_CREATE);

        // Expected product name
        String expectedName = JsonPath.read(productData, JSON_PATH_NAME);

        // Start create operation
        mvc.perform(MockMvcRequestBuilders.post(PRODUCT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(productData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(HttpStatus.CREATED.value()),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_PATH_ID).isString(),
                        MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(expectedName)
                ));
    }

    @Test
    void givenEmptyNameInProductData_whenPostOperation_thenReturnError() throws Exception {
        String productData = loadJsonData(DATA_CREATE);
        productData = JsonPath
                .parse(productData)
                .set(JSON_PATH_NAME, EMPTY_STRING)
                .jsonString();

        // Expected error response
        String expectedMessage = "Product name field cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(PRODUCT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(productData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenNullNameInProductData_whenPostOperation_thenReturnError() throws Exception {
        String productData = loadJsonData(DATA_CREATE);
        productData = JsonPath
                .parse(productData)
                .delete(JSON_PATH_NAME)
                .jsonString();

        // Expected error response
        String expectedMessage = "Product name field cannot be empty";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(PRODUCT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(productData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenProductDataWithInventoryUnderZero_whenPostOperation_thenReturnError() throws Exception {
        String productData = loadJsonData(DATA_CREATE);
        productData = JsonPath
                .parse(productData)
                .set(JSON_PATH_INVENTORY, -1)
                .jsonString();

        // Expected error response
        String expectedMessage = "Inventory value at least zero";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(PRODUCT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(productData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenProductDataWithNullInventory_whenPostOperation_thenReturnError() throws Exception {
        String productData = loadJsonData(DATA_CREATE);
        productData = JsonPath
                .parse(productData)
                .delete(JSON_PATH_INVENTORY)
                .jsonString();

        // Expected error response
        String expectedMessage = "Inventory value at least zero";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(PRODUCT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(productData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenUndefinedFieldInProductData_whenPostOperation_thenReturnError() throws Exception {
        String productData = loadJsonData(DATA_CREATE);
        productData = JsonPath
                .parse(productData)
                .put("$", "dummyField", "dummyValue")
                .jsonString();

        // Expected error response
        String expectedMessage = "JSON parse error: Unrecognized field";
        int expectedStatus = HttpStatus.BAD_REQUEST.value();

        mvc.perform(MockMvcRequestBuilders.post(PRODUCT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(productData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedProduct_whenGetOperation_thenReturnSuccess() throws Exception {
        String productData = loadJsonData(DATA_CREATE);

        createPOSTRequest(PRODUCT_URL, productData)
                .andDo(mvcResult -> {
                    String productId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    String productName = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_NAME);
                    mvc.perform(MockMvcRequestBuilders.get(PRODUCT_URL + SLASH + productId)
                            .header(ACCEPT, APPLICATION_JSON)
                            .header(CONTENT_TYPE, APPLICATION_JSON))
                            .andExpect(ResultMatcher.matchAll(
                                    status().is(HttpStatus.OK.value()),
                                    status().is2xxSuccessful(),
                                    header().exists(HttpHeaders.CONTENT_TYPE),
                                    jsonPath(JSON_PATH, notNullValue()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_ID).value(productId),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(productName)
                            ));
                });
    }

    @Test
    void givenNotCreatedProduct_whenGetOperation_thenReturnError() throws Exception {
        String expectedMessage = "No product with id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.get(PRODUCT_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedProduct_whenDeleteOperation_thenReturnSuccess() throws Exception {
        String productData = loadJsonData(DATA_CREATE);

        createPOSTRequest(PRODUCT_URL, productData)
                .andDo(mvcResult -> {
                    String productId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    mvc.perform(MockMvcRequestBuilders.delete(PRODUCT_URL + SLASH + productId))
                            .andExpect(ResultMatcher.matchAll(
                                    status().is(HttpStatus.NO_CONTENT.value()),
                                    status().is2xxSuccessful()
                            ));
                });
    }

    @Test
    void givenNotCreatedProduct_whenDeleteOperation_thenReturnError() throws Exception {
        String expectedMessage = "No product with id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.delete(PRODUCT_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedProduct_whenPutOperation_thenReturnSuccess() throws Exception {
        String productData = loadJsonData(DATA_CREATE);

        createPOSTRequest(PRODUCT_URL, productData)
                .andDo(mvcResult -> {
                    String productId = JsonPath.read(mvcResult.getResponse().getContentAsString(), JSON_PATH_ID);
                    String productUpdateData = loadJsonData(DATA_CREATE);
                    String expectedName = "name" + UUID.randomUUID().toString();
                    productUpdateData = JsonPath.parse(productUpdateData)
                            .set(JSON_PATH_NAME, expectedName)
                            .delete(JSON_PATH_INVENTORY).jsonString();

                    mvc.perform(MockMvcRequestBuilders.put(PRODUCT_URL + SLASH + productId)
                            .header(ACCEPT, APPLICATION_JSON)
                            .header(CONTENT_TYPE, APPLICATION_JSON)
                            .content(productUpdateData))
                            .andExpect(ResultMatcher.matchAll(
                                    result -> System.out.println(result.getResponse().getContentAsString()),
                                    status().is(HttpStatus.OK.value()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_ID).value(productId),
                                    MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).value(expectedName)
                            ));
                });
    }

    @Test
    void givenNotCreatedProduct_whenPutOperation_thenReturnError() throws Exception {
        String productUpdateData = loadJsonData(DATA_CREATE);

        String expectedMessage = "No product with id";
        int expectedStatus = HttpStatus.NOT_FOUND.value();

        mvc.perform(MockMvcRequestBuilders.put(PRODUCT_URL + SLASH + UNDEFINED_ID)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(productUpdateData))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(expectedStatus),
                        MockMvcResultMatchers.jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_MESSAGE, containsString(expectedMessage)),
                        MockMvcResultMatchers.jsonPath(JSON_STATUS).value(expectedStatus)
                ));
    }

    @Test
    void givenCreatedProductList_whenGetListOperation_thenReturnSuccess() throws Exception {
        String productData = loadJsonData(DATA_CREATE);

        // Create first product
        createPOSTRequest(PRODUCT_URL, productData)
                .andDo(mvcResult -> {

                    // Create second product
                    createPOSTRequest(PRODUCT_URL, productData)
                            .andDo(mvcRes -> mvc.perform(MockMvcRequestBuilders.get(PRODUCT_URL)
                                    .header(ACCEPT, APPLICATION_JSON)
                                    .header(CONTENT_TYPE, APPLICATION_JSON))
                                    .andExpect(ResultMatcher.matchAll(
                                            status().is(HttpStatus.OK.value()),
                                            status().is2xxSuccessful(),
                                            header().exists(HttpHeaders.CONTENT_TYPE),
                                            jsonPath(JSON_PATH, notNullValue()),
                                            jsonPath(JSON_PATH_ARRAY, hasSize(2))
                                    )));
                });
    }

}
