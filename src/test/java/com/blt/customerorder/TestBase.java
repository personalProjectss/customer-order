package com.blt.customerorder;

import com.blt.customerorder.repository.CustomerRepository;
import com.blt.customerorder.repository.OrderRepository;
import com.blt.customerorder.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureMockMvc
@SpringBootTest(classes = {CustomerOrderApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestBase {

    protected final String PRODUCT_URL = "/product";
    protected final String CUSTOMER_URL = "/customer";
    protected final String ORDER_URL = "/order";
    protected final String SLASH = "/";
    protected final String EMPTY_STRING = " ";
    protected final String UNDEFINED_ID = "404ID";

    protected final String APPLICATION_JSON = "application/json;charset=utf-8";
    protected final String CONTENT_TYPE = "Content-Type";
    protected final String ACCEPT = "Accept";
    protected final String JSON_PATH = "$";
    protected final String JSON_PATH_ARRAY = "$[*]";
    protected final String JSON_PATH_ID = "$.id";
    protected final String JSON_PATH_NAME = "$.name";
    protected final String JSON_PATH_lAST_NAME = "$.lastName";
    protected final String JSON_PATH_EMAIL = "$.email";
    protected final String JSON_PATH_PHONE = "$.phone";
    protected final String JSON_MESSAGE = "$.message";
    protected final String JSON_STATUS = "$.status";

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected ObjectMapper objectMapper;

    public ResultActions createPOSTRequest(String url, String data) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.post(url)
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .content(data))
                .andExpect(ResultMatcher.matchAll(
                        result -> System.out.println(result.getResponse().getContentAsString()),
                        status().is(HttpStatus.CREATED.value()),
                        jsonPath(JSON_PATH, notNullValue()),
                        MockMvcResultMatchers.jsonPath(JSON_PATH_ID).isString(),
                        MockMvcResultMatchers.jsonPath(JSON_PATH_NAME).isString()
                ));
    }

    @AfterEach
    void clean() {
        productRepository.deleteAll();
        customerRepository.deleteAll();
        orderRepository.deleteAll();
    }

    public String loadJsonData(String jsonFile) throws Exception {
        ClassLoader classLoader = TestBase.class.getClassLoader();
        File file = FileUtils.toFile(classLoader.getResource(jsonFile));
        if (file == null) {
            throw new IOException("File is not found!");
        }
        return FileUtils.readFileToString(file, "UTF-8");
    }
}
