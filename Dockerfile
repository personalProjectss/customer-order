FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /app
COPY target/customer-order-0.0.1.jar ./
ENTRYPOINT ["java","-jar","customer-order-0.0.1.jar"]