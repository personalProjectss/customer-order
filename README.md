# customer-order ![code-coverage](https://img.shields.io/badge/coverage-97%25-brightgreen) ![passed-tests](https://img.shields.io/badge/passed%20test-54-blue)

## Description

This project is simply includes `Customer`, `Product` and `Order` among relationship. This project created by using
spring boot framework. It supports MySql, PostgreSql, H2Database, Oracle Database drivers. You can change the driver
name and data source url according to your desired database. The project uses H2 database as default. It uses `8080`
port. If you want to change this port number or other configuration after docker deployment, you need to
change `server.port` variable in `.env` file under `deploy` folder, and you should restart deployment.

## Project Scenario

- The project has 3 main entities that are `Customer`, `Product` and `Order`.
- Each customer can have one or more order.
- Each product can have one or more order.
- Product must have number of stock as inventory to create an order. Otherwise, order couldn't be created.
- An order must have one customer and one product information.
- The number of orders must be one or more.

### Note

You can find more information about the model and API in the Swagger documentation
at http://localhost:8080/swagger-ui.html.

## Build And Run

Following steps must be done to run the project.

1. Build the project

```shell
mvn clean install
```

2. Build and run it as docker.

```shell
docker-compose up --build
```